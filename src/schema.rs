// @generated automatically by Diesel CLI.

diesel::table! {
    stops (id) {
        id -> Integer,
        ril100 -> Text,
        stop_type -> Integer,
    }
}
