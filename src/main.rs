#![macro_use]

use rocket::{routes, launch, Rocket, Build, get, post, serde::Deserialize, serde::json::Json}; 
use travelstats::models::Stops;
use diesel::prelude::*;
use travelstats::*;

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
#[derive(Debug)]
struct Station {
    name: String,
    ds100: String,
    #[serde(rename(deserialize = "scheduledTime"))]
    scheduled_time: i64,
    #[serde(rename(deserialize = "realTime"))]
    real_time: i64,
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
#[derive(Debug)]
struct UserStatus {
    deprecated: bool,
    #[serde(rename(deserialize = "toStation"))]
    to_station: Station,
    #[serde(rename(deserialize = "fromStation"))]
    from_station: Station,
}

#[derive(Deserialize)]
#[serde(crate = "rocket::serde")]
#[derive(Debug)]
struct Travelynx {
    reason: String,
    status: UserStatus,
}

#[get("/")]
fn hello() -> String {
    format!("Hello, world!")
}

#[post("/travelynx", data = "<status>")]
fn webhook_travelynx(status: Json<Travelynx>) -> String {
    println!("{:#?}", status);
    let connection = &mut establish_connection();
    let post = create_stop(connection, &status.status.from_station.ds100, &1);
    format!("Ok")

}

#[launch]
fn rocket() -> Rocket<Build> {
    rocket::build()
    .mount("/hello", routes![hello])
    .mount("/webhook", routes![webhook_travelynx])
}