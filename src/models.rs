use diesel::prelude::*;
use super::schema::stops;

#[derive(Queryable, Selectable)]
#[diesel(table_name = stops)]
#[diesel(check_for_backend(diesel::sqlite::Sqlite))]
pub struct Stops {
    pub id: i32,
    pub ril100: String,
    pub stop_type: i32,
}

#[derive(Insertable)]
#[diesel(table_name = stops)]
pub struct NewStops<'a> {
    pub ril100: &'a str,
    pub stop_type: &'a i32,
}