pub mod models;
pub mod schema;

use diesel::prelude::*;
use models::Stops;
use self::models::NewStops;

pub fn establish_connection() -> SqliteConnection {
    let database_url = "database.sqlite";
    SqliteConnection::establish(database_url)
        .unwrap_or_else(|_| panic!("Error connecting to database: {}", database_url))
}

pub fn create_stop(conn: &mut SqliteConnection, ril100: &str, stop_type: &i32) -> Stops {
    use crate::schema::stops;

    let new_stops = NewStops {ril100, stop_type};

    diesel::insert_into(stops::table)
        .values(&new_stops)
        .returning(Stops::as_returning())
        .get_result(conn)
        .expect("Error saving stop")
}